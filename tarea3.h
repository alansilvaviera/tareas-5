#include<iostream>
#include<cstring>
using namespace std;

void imprimeNombre(char* nombre){
	cout << "Hola: " << nombre << " la longitud es: " << strlen(nombre) << endl;
}

float operacion(int a, int b, int codigo){
	if( codigo == 1 ){
		return (float)a + b;
	} else if( codigo == 2 ){
		return (float)a - b;
	}else if( codigo == 3 ){
		return (float)a * b;
	}else if( codigo == 4 ){
		if( b == 0 ){
			return (float) -1;
		}else{
		    return (float) a / b;	
		}
	}else {
		cout << "Codigo no valido, por favor elige un codigo valido" << endl; 
		return -1;
	}
}
